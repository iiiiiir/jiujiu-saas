import { defineConfig } from 'umi';
import routes from './config/routes';

export default defineConfig({
  define: {
    // 后台访问地址
    // 'process.env.baseUrl': 'http://2f22-36-161-237-201.ngrok.io',//远程调试
    // 'process.env.baseUrl': 'http://api.s11edao.com/gateway',
    'process.env.baseUrl': 'http://127.0.0.1:8097/gateway', //本地调试
  },
  nodeModulesTransform: {
    type: 'none',
  },
  // 浏览器兼容
  targets: {
    ie: 11,
  },
  // 开启配置qiankun
  qiankun: {
    master: {
      // 注册权限管理信息
      apps: [
        {
          name: 'bsin-ui-upms', // 唯一 id
          entry: 'http://localhost:8003', // html entry
        },
      ],
      routes: [
        {
          path: '/bsin-ui-upms',
          microApp: 'bsin-ui-upms',
        },
      ],
    },
    slave: {},
  },
  // 路由模式
  history: { type: 'hash' },
  // 开启dva
  dva: {
    // 开启dva-immer，用于代理currentState和nextState之间的改变，即当前状态修改副本
    immer: true,
    // 开启模块热加载(热更新)
    hmr: true,
  },
  // 开启antd
  antd: {},
  // 路由  可单独抽离  这里的路由配置默认走layouts ，可根据项目组需求选择删除
  routes,
  // 打包后资源路径
  publicPath: './',
});
